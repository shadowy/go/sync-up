package main

import (
	"fmt"
	"os"
	"path/filepath"
)

func main() {
	_ = filepath.Walk("/home/aleksey", func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			fmt.Println(path)
		}
		return nil
	})
}
