package storage

type Folder struct {
	Name         string  `yaml:"name"`
	Modification int64   `yaml:"mod"`
	Schema       *Schema `yaml:"schema"`
}
