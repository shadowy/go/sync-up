package storage

type Schema struct {
	Folders *[]Folder `yaml:"folders"`
	Files   *[]File   `yaml:"files"`
}

func (schema *Schema) LoadFromFile(filename string) error {

}

func (schema *Schema) LoadFromReader(filename string) error {

}

func (schema *Schema) LoadFromString(filename string) error {

}

func LoadSchema(filename string) (schema *Schema, err error) {
	schema = new(Schema)
}
