package storage

type File struct {
	Name         string `yaml:"name"`
	Modification int64  `yaml:"mod"`
}
